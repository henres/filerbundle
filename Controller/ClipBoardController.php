<?php

namespace Henres\FilerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\File\File as sFile;
use Henres\FilerBundle\Form\ClipBoardType;
use Henres\FilerBundle\Form\FileType;
use Henres\FilerBundle\Document\ClipBoard;
use Henres\FilerBundle\Document\ClipBoardItem;
use Henres\FilerBundle\Document\File;
use Henres\FilerBundle\Document\Folder;

class ClipBoardController extends Controller
{
    /**
     * Creates a new FileUtem document.
     *
     * @Route("/admin/filer/file/create", name="page_create")
     * @Method("POST")
     * @Template("HenresFilerBundle:Admin:index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createFileAction(Request $request, $folder = null) {

        $document = new File();
        $form     = $this->createForm(new FileType(), $document);
        $form->bind($request);

        $session = $this->getRequest()->getSession();
        $sclipboard = $session->get('clipboard');

        $dm = $this->getDocumentManager();
        $clipboard = $dm->getRepository('HenresFilerBundle:ClipBoard')->find($sclipboard->getId());
        
        $clipboard->setUser($this->getUser());
        // $dm->persist($clipboard);
        // $dm->flush();

        if ($form->isValid()) {
            $file = $form['file']->getData();

            $document->setFile($file->getPathname());
            $document->setFilename($file->getClientOriginalName());
            $document->setMimeType($file->getClientMimeType());
            $document->setOwner($this->getUser()->getId());
            
            $config = $this->container->getParameter('henres_filer.config');
            
            $dm->persist($document);
            $dm->flush();
            $clipboard->addFile($document);

            $dm->persist($clipboard);
            $dm->flush();

            $clipboard = $session->set('clipboard', $clipboard);

            if ($folder) {

                return $this->redirect($this->generateUrl('admin_filer_folder_index', array('folderid' => $folder)));
            } else {
                return $this->redirect($this->generateUrl('admin_filer_index'));
            } 
        }
        //return new Response('Hello world!');
        return $this->redirect($this->generateUrl('admin_filer_index'));
    }

    public function emptyClipBoardAction(Request $request, $folder = null) {

        $session = $this->getRequest()->getSession();
        $clipboard = $session->get('clipboard');
        $dm = $this->getDocumentManager();
        $clipboard = $dm->getRepository('HenresFilerBundle:ClipBoard')->find($clipboard->getId());
        
        $dm->remove($clipboard);
        $dm->flush();

        $response = $this->forward('HenresFilerBundle:Admin:index', array('folder' => $folder));

        return $response;
    }

    public function pasteAction(Request $request, $id) {

        $session = $this->getRequest()->getSession();
        $clipboard = $session->get('clipboard');
        $dm = $this->getDocumentManager();

        $clipboard = $dm->getRepository('HenresFilerBundle:ClipBoard')->find($clipboard->getId());

        $folder = $dm->getRepository('HenresFilerBundle:Folder')->find($id);

        $files = $clipboard->getFiles();

        foreach ($files as $file) {

            $file->setFolder($folder);
            $dm->persist($file);
            $dm->flush();
        }

        $dm->remove($clipboard);
        $dm->flush();

        return $this->redirect($this->generateUrl('admin_filer_folder_index', array('folderid' => $folder->getId())));
    }

    private function generateAllSize () {

        
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}