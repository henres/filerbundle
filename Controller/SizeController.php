<?php

namespace Henres\FilerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Henres\FilerBundle\Form\ClipBoardType;
use Henres\FilerBundle\Document\File;
use Henres\FilerBundle\Form\FileType;
use Henres\FilerBundle\Document\ClipBoard;
use Henres\FilerBundle\Document\Folder;
use Henres\FilerBundle\Document\ClipBoardItem;

class SizeController extends Controller
{

    /**
     * Displays a form to create a new File document.
     *
     * @Route("/new", name="file_new")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $document = new File();
        $form = $this->createForm(new FileType(), $document);

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new File document.
     *
     * @Route("/create", name="file_create")
     * @Method("POST")
     * @Template("SiteFilerBundle:File:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new File();
        $form     = $this->createForm(new FileType(), $document);
        $form->bind($request);

        if ($form->isValid()) {
            $file = $form['file']->getData();

            $document->setFile($file->getPathname());
            $document->setFilename($file->getClientOriginalName());
            $document->setMimeType($file->getClientMimeType());

            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('file_show', array('id' => $document->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    public function updateName(Request $request) {
        return true;
    }

    /**
     * Deletes a File document.
     *
     * @Route("/{id}/delete", name="file_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('SiteFilerBundle:File')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find File document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('file'));
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}