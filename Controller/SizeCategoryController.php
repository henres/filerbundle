<?php

namespace Henres\FilerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\File\File as sFile;
use Henres\FilerBundle\Document\File;
use Henres\FilerBundle\Form\FileType;

/**
 * File controller.
 *
 * @Route("/file")
 */
class SizeCategoryController extends Controller
{
    /**
     * Lists all File documents.
     *
     * @Route("/", name="file")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('SiteFilerBundle:File')->findAll();

        return array('documents' => $documents);
    }

    /**
     * Displays a form to create a new File document.
     *
     * @Route("/new", name="file_new")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $document = new File();
        $form = $this->createForm(new FileType(), $document);

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new File document.
     *
     * @Route("/create", name="file_create")
     * @Method("POST")
     * @Template("SiteFilerBundle:File:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new File();
        $form     = $this->createForm(new FileType(), $document);
        $form->bind($request);

        if ($form->isValid()) {
            $file = $form['file']->getData();

            $document->setFile($file->getPathname());
            $document->setFilename($file->getClientOriginalName());
            $document->setMimeType($file->getClientMimeType());
            //$document->setOwner($this->getUser());

            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('file_show', array('id' => $document->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Finds and displays a File document.
     *
     * @Route("/{id}/show", name="file_show")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function showAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('SiteFilerBundle:File')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing File document.
     *
     * @Route("/{id}/edit", name="file_edit")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('SiteFilerBundle:File')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $editForm = $this->createForm(new FileType(), $document);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing File document.
     *
     * @Route("/{id}/update", name="file_update")
     * @Method("POST")
     * @Template("SiteFilerBundle:File:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('SiteFilerBundle:File')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new FileType(), $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('file_edit', array('id' => $id)));
        }

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a File document.
     *
     * @Route("/{id}/delete", name="file_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('SiteFilerBundle:File')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find File document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('file'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function fileshowAction($id) {

        $dm = $this->getDocumentManager();

        $file = $dm->getRepository('SiteFilerBundle:File')
                    ->find($id);
 
        if (null === $file) {
            throw $this->createNotFoundException(sprintf('Upload with id "%s" could not be found', $id));
        }
     
        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
     
        $response->setContent($file->getFile()->getMongoGridFSFile()->getBytes());
     
        return $response;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
