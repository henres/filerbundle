<?php

namespace Henres\FilerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Henres\FilerBundle\Document\Folder;
use Henres\FilerBundle\Form\FolderType;
use Henres\FilerBundle\Form\FolderEditType;

/**
 * Folder controller.
 *
 * @Route("/folder")
 */
class FolderController extends Controller
{
    /**
     * Lists all Folder documents.
     *
     * @Route("/", name="folder")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('HenresFilerBundle:Folder')->findAll();

        return array('documents' => $documents);
    }

    /**
     * Displays a form to create a new Folder document.
     *
     * @Route("/new", name="folder_new")
     * @Template()
     *
     * @return array
     */
    public function newAction($parent)
    {
        $dm = $this->getDocumentManager();
        $document = new Folder();
        $parent = $dm->getRepository('HenresFilerBundle:Folder')->find($parent);
        $parent->addChildren($document);
        $document->setParent($parent);
        $form = $this->createForm(new FolderType(), $document);

        return array(
            'parent' => $parent->getId(),
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new Folder document.
     *
     * @Route("/create", name="folder_create")
     * @Method("POST")
     * @Template("HenresFilerBundle:Folder:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request, $parent)
    {
        $document = new Folder();
        $form     = $this->createForm(new FolderType(), $document);
        $form->bind($request);

        if ($form->isValid()) {

            $dm = $this->getDocumentManager();
            $parent = $dm->getRepository('HenresFilerBundle:Folder')->find($parent);

            if (function_exists ($this->getUser()->getId())) {
                $document->setOwner($this->getUser()->getId());
            }
            
            $document->setParent($parent);

            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_filer_folder_index', array('folderid' => $parent->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Finds and displays a File document.
     *
     * @Route("/{id}/show", name="file_show")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function showAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('HenresFilerBundle:File')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing File document.
     *
     * @Route("/{id}/edit", name="file_edit")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('HenresFilerBundle:Folder')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $editForm = $this->createForm(new FolderEditType(), $document);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing File document.
     *
     * @Route("/{id}/update", name="file_update")
     * @Method("POST")
     * @Template("HenresFilerBundle:File:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('HenresFilerBundle:Folder')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find File document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new FolderEditType(), $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_filer_folder_index', array('folderid' => $document->getId())));
        }

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Folder document.
     *
     * @Route("/{id}/delete", name="file_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {

        $dm = $this->getDocumentManager();
        $document = $dm->getRepository('HenresFilerBundle:Folder')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Folder document.');
        }

        $dm->remove($document);
        $dm->flush();

        return $this->redirect($this->generateUrl('admin_filer_index'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function fileshowAction($id) {

        $dm = $this->getDocumentManager();

        $file = $dm->getRepository('HenresFilerBundle:File')
                    ->find($id);
 
        if (null === $file) {
            throw $this->createNotFoundException(sprintf('Upload with id "%s" could not be found', $id));
        }
     
        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
     
        $response->setContent($file->getFile()->getMongoGridFSFile()->getBytes());
     
        return $response;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
