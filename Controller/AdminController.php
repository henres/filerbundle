<?php

namespace Henres\FilerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Henres\FilerBundle\Form\ClipBoardType;
use Henres\FilerBundle\Form\FileType;
use Henres\FilerBundle\Document\ClipBoard;
use Henres\FilerBundle\Document\Folder;
use Henres\FilerBundle\Document\ClipBoardItem;
use Henres\FilerBundle\Document\File;

class AdminController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction(Request $request, $folderid = "") {

        $config = $this->container->getParameter('henres_filer.config');
        $session = $this->getRequest()->getSession();
        $dm = $this->getDocumentManager();
        $clipboard = null;
        $sclipboard = $session->get('clipboard');
        if($sclipboard) {
            
            $clipboard = $dm->getRepository('HenresFilerBundle:ClipBoard')->find($sclipboard->getId());
        }
        
        if ($folderid != "") {
            
            $folder = $dm->getRepository('HenresFilerBundle:Folder')->find($folderid);
        } else {
            $folder = $dm->getRepository('HenresFilerBundle:Folder')->findOneByName('root');
        }
        
        if (!$clipboard) {
            $clipboard = new ClipBoard();
            $clipboard->setUser($this->getUser());
            $dm->persist($clipboard);
            $dm->flush();
            $session->set('clipboard', $clipboard);
        }
        if (!$folder) {
            $folder = new Folder('root');
            $dm->persist($folder);
            $dm->flush();
        }
        
        $file = new File();
        $fileform = $this->createForm(new FileType(), $file, array(
            'action' => $this->generateUrl('admin_file_create'),
            'method' => 'POST',
        ));
        
        $content = $this->renderView(
            'HenresFilerBundle:Admin:index.html.twig',
            array(
                'clipboard' => $clipboard,
                'fileform' => $fileform->createView(),
                'folder' => $folder,
                'template' => $config['template']['admin']
            )
        );

        return new Response($content);
    }

    public function ckeditorAction(Request $request, $folderid = "") {

        $session = $this->getRequest()->getSession();
        $ckeditor = $request->query->get('CKEditor');
        $funcname = $request->query->get('CKEditorFuncNum');
        $sckeditor = $session->get('ckeditor');

        if($ckeditor || $sckeditor ) {

            $template = 'HenresFilerBundle:Admin:ckeditor.html.twig';
            if (empty($sckeditor)) {
                
                $arCkeditor = array(
                    'CKEditor' => $request->query->get('CKEditor'),
                    'CKEditorFuncNum' => $request->query->get('CKEditorFuncNum')
                );
                $session->set('ckeditor', $arCkeditor);
            }
        } else {

            $template = 'HenresFilerBundle:Admin:index.html.twig';
        }

        $dm = $this->getDocumentManager();
        $clipboard = null;
        $sclipboard = $session->get('clipboard');
        if($sclipboard) {

            $clipboard = $dm->getRepository('HenresFilerBundle:ClipBoard')->find($sclipboard->getId());
        }

        if ($folderid != "") {
            
            $folder = $dm->getRepository('HenresFilerBundle:Folder')->find($folderid);
        } else {
            $folder = $dm->getRepository('HenresFilerBundle:Folder')->findOneByName('root');
        }

        if (!$clipboard) {
            $clipboard = new ClipBoard();
            $clipboard->setUser($this->getUser());
            $dm->persist($clipboard);
            $dm->flush();
            $session->set('clipboard', $clipboard);
        }
        if (!$folder) {
            $folder = new Folder('root');
            $dm->persist($folder);
            $dm->flush();
        }
        
        $file = new File();
        $fileform = $this->createForm(new FileType(), $file, array(
            'action' => $this->generateUrl('file_create_admin'),
            'method' => 'POST',
        ));
        
        $content = $this->renderView(
            $template,
            array(
                'clipboard' => $clipboard,
                'fileform' => $fileform->createView(),
                'folder' => $folder
            )
        );

        return new Response($content);
    }

    public function remCkeditorFromSessionAction() {

        $session = $this->getRequest()->getSession();
        $session->remove('ckeditor');

        return New Response('true');
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}