<?php

namespace Henres\FilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Henres\FilerBundle\Document\File;
use Henres\FilerBundle\Form\FileType;

class FileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Henres\FilerBundle\Document\File'
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Henres\FilerBundle\Document\File',
        );
    }

    public function getName()
    {
        return 'henres_filerbundle_filetype';
    }
}
