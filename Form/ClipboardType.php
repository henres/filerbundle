<?php

namespace Henres\FilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Henres\FilerBundle\Document\File;

class ClipBoardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('files', 'collection', array(
                'type'=>new FileType(),
                'allow_add'=>true,
                'data'=>array(new File)
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Henres\FilerBundle\Document\ClipBoard'
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Henres\FilerBundle\Document\ClipBoard',
        );
    }

    public function getName()
    {
        return 'henres_cmsbundle_clipboardtype';
    }
}
