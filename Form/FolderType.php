<?php

namespace Henres\FilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Henres\FilerBundle\Document\Folder;

class FolderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Henres\FilerBundle\Document\Folder'
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Henres\FilerBundle\Document\Folder',
        );
    }

    public function getName()
    {
        return 'henres_cmsbundle_foldertype';
    }
}
