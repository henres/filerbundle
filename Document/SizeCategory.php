<?php

namespace Henres\FilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class SizeCategory {
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $name;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Size", strategy="addToSet", mappedBy="type", cascade={"remove"})
     */
    private $sizes;
    
    public function __construct()
    {
        $this->sizes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add size
     *
     * @param Henres\FilerBundle\Document\Size $size
     */
    public function addSize(\Henres\FilerBundle\Document\Size $size)
    {
        $this->sizes[] = $size;
    }

    /**
     * Remove size
     *
     * @param Henres\FilerBundle\Document\Size $size
     */
    public function removeSize(\Henres\FilerBundle\Document\Size $size)
    {
        $this->sizes->removeElement($size);
    }

    /**
     * Get sizes
     *
     * @return Doctrine\Common\Collections\Collection $sizes
     */
    public function getSizes()
    {
        return $this->sizes;
    }
}
