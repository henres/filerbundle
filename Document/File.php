<?php

namespace Henres\FilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class File {
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\File
     */
    private $file;

    /** 
     * @MongoDB\String
     */
    private $filename;
    
    /**
     * @MongoDB\String
     */
    private $mimeType;

    /**
     * @MongoDB\Int 
     */
    private $chunkSize;

    /**
     * @MongoDB\Int 
     */
    private $length;
 
    /**
     * @MongoDB\String 
     */
    private $md5;

    /*
     * @MongoDB\Boolean(default=0)
     */
    protected $original_size;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Size", strategy="addToSet", mappedBy="type", cascade={"remove"})
     */
    protected $childs;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Size", inversedBy="files")
     */
    private $img_size;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Folder", inversedBy="files")
     */
    private $folder;

    /**
     * @MongoDB\String
     */
    private $owner;

    /**
     * @MongoDB\Date
     */
    private $uploadDate;

    /**
     * @MongoDB\Boolean
     */
    private $is_resized;

    /**
     * @MongoDB\Boolean
     */
    private $is_orignal;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Henres\FilerBundle\Document\File")
     */
    private $original;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param file $file
     * @return self
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * Get file
     *
     * @return file $file
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return self
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get filename
     *
     * @return string $filename
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return self
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * Get owner
     *
     * @return string $owner
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return self
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string $mimeType
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set chunkSize
     *
     * @param int $chunkSize
     * @return self
     */
    public function setChunkSize($chunkSize)
    {
        $this->chunkSize = $chunkSize;
        return $this;
    }

    /**
     * Get chunkSize
     *
     * @return int $chunkSize
     */
    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    /**
     * Set length
     *
     * @param int $length
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * Get length
     *
     * @return int $length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return self
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;
        return $this;
    }

    /**
     * Get md5
     *
     * @return string $md5
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set folder
     *
     * @param Henres\FilerBundle\Document\Folder $folder
     * @return self
     */
    public function setFolder(\Henres\FilerBundle\Document\Folder $folder)
    {
        $this->folder = $folder;
        return $this;
    }

    /**
     * Get folder
     *
     * @return Site\FilerBundle\Document\Folder $folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set uploadDate
     *
     * @param date $uploadDate
     * @return self
     */
    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;
        return $this;
    }

    /**
     * Get uploadDate
     *
     * @return date $uploadDate
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }
    public function __construct()
    {
        $this->childs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add child
     *
     * @param Site\FilerBundle\Document\Size $child
     */
    public function addChild(\Henres\FilerBundle\Document\Size $child)
    {
        $this->childs[] = $child;
    }

    /**
     * Remove child
     *
     * @param Henres\FilerBundle\Document\Size $child
     */
    public function removeChild(\Henres\FilerBundle\Document\Size $child)
    {
        $this->childs->removeElement($child);
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection $childs
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * Set imgSize
     *
     * @param Henres\FilerBundle\Document\Size $imgSize
     * @return self
     */
    public function setImgSize(\Henres\FilerBundle\Document\Size $imgSize)
    {
        $this->img_size = $imgSize;
        return $this;
    }

    /**
     * Get imgSize
     *
     * @return Site\FilerBundle\Document\Size $imgSize
     */
    public function getImgSize()
    {
        return $this->img_size;
    }

    /**
     * Set isResized
     *
     * @param boolean $isResized
     * @return self
     */
    public function setIsResized($isResized)
    {
        $this->is_resized = $isResized;
        return $this;
    }

    /**
     * Get isResized
     *
     * @return boolean $isResized
     */
    public function getIsResized()
    {
        return $this->is_resized;
    }

    /**
     * Set isOrignal
     *
     * @param boolean $isOrignal
     * @return self
     */
    public function setIsOrignal($isOrignal)
    {
        $this->is_orignal = $isOrignal;
        return $this;
    }

    /**
     * Get isOrignal
     *
     * @return boolean $isOrignal
     */
    public function getIsOrignal()
    {
        return $this->is_orignal;
    }

    /**
     * Set original
     *
     * @param Henres\FilerBundle\Document\File $original
     * @return self
     */
    public function setOriginal(\Henres\FilerBundle\Document\File $original)
    {
        $this->original = $original;
        return $this;
    }

    /**
     * Get original
     *
     * @return Henres\FilerBundle\Document\File $original
     */
    public function getOriginal()
    {
        return $this->original;
    }
}
