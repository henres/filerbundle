<?php

namespace Henres\FilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="Henres\FilerBundle\Repository\ClipBoardRepository")
 * @Gedmo\Loggable
 */
class ClipBoard
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    protected $user;

    /**
     * @MongoDB\ReferenceMany(
     * targetDocument="File", 
     * strategy="addToSet",
     *   discriminatorMap={
     *     "file"="File",   
     *     "image"="Image"
     *   }
     * )
     */
    protected $files = array();

    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add file
     *
     * @param $file
     */
    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    /**
     * Remove file
     *
     * @param $file
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return Doctrine\Common\Collections\Collection $files
     */
    public function getFiles()
    {
        return $this->files;
    }
    
    public function setUser($user) {
        
        $this->user = $user;
        
        return $this;
    }
    
    public function getUser() {
        
        return $this->user;
    }
} 
