<?php

namespace Henres\FilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class Folder
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Int
     */
    protected $order;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\String
     */
    protected $description;

    /**
     * @MongoDB\ReferenceMany(targetDocument="File", strategy="addToSet", mappedBy="folder", cascade={"remove"})
     */
    protected $files = array();

    /**
     * @MongoDB\String
     */
    protected $title;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Folder", inversedBy="children") 
     */
    private $parent;

    /**
     * @MongoDB\String
     */
    private $owner;
    
    /**
     * @MongoDB\ReferenceMany(targetDocument="Folder", mappedBy="parent", cascade={"remove"}) 
     */
    private $childrens;

    /**
     * @Gedmo\Timestampable(on="create")
     * @MongoDB\Date
     */
    protected $created_at;

    /**
     * @Gedmo\Timestampable(on="update")
     * @MongoDB\Date
     */
    protected $updated_at;

    public function __construct($title = '')
    {   
        if ($title != '') {
            $this->title = $title;
            $this->name = $title;
            $this->order = 0;
        }
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->childrens = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {

        return $this->title;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add file
     *
     * @param Henres\FilerBundle\Document\File $file
     */
    public function addFile(\Henres\FilerBundle\Document\File $file)
    {
        $this->files[] = $file;
    }

    /**
     * Remove file
     *
     * @param Henres\FilerBundle\Document\File $file
     */
    public function removeFile(\Henres\FilerBundle\Document\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return Doctrine\Common\Collections\Collection $files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param Henres\FilerBundle\Document\Folder $parent
     * @return self
     */
    public function setParent(\Henres\FilerBundle\Document\Folder $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return Site\FilerBundle\Document\Folder $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param Henres\FilerBundle\Document\Folder $children
     */
    public function addChildren(\Henres\FilerBundle\Document\Folder $children)
    {
        $this->childrens[] = $children;
    }

    /**
     * Remove children
     *
     * @param Henres\FilerBundle\Document\Folder $children
     */
    public function removeChildren(\Henres\FilerBundle\Document\Folder $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return Doctrine\Common\Collections\Collection $childrens
     */
    public function getChildrens()
    {
        return $this->childrens;
    }

    /**
     * Set createdAt
     *
     * @param date $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param date $updatedAt
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return date $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setOwner($user) {
        
        $this->owner = $user;
        
        return $this;
    }
    
    public function getOwner() {
        
        return $this->owner;
    }
}
